package co.id.keda87.mobbudget.preference;

import android.os.Bundle;
import android.preference.PreferenceFragment;
import co.id.keda87.mobbudget.R;

/**
 * Created with IntelliJ IDEA.
 * User: Keda87
 * Date: 12/22/13
 * Time: 8:14 PM
 */
public class SettingPreference extends PreferenceFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences_settings);
    }
}
