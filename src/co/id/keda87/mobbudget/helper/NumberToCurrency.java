package co.id.keda87.mobbudget.helper;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

/**
 * Created with IntelliJ IDEA.
 * User: Keda87
 * Date: 12/23/13
 * Time: 2:37 AM
 */
public class NumberToCurrency {

    public static String currencyBuilder(long cost) {
        DecimalFormat df = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setCurrencySymbol("Rp. ");
        dfs.setMonetaryDecimalSeparator(',');
        dfs.setGroupingSeparator('.');
        df.setDecimalFormatSymbols(dfs);
        return df.format(cost);
    }

}
