package co.id.keda87.mobbudget.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import co.id.keda87.mobbudget.pojo.Daily;
import co.id.keda87.mobbudget.pojo.Monthly;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Keda87
 * Date: 12/21/13
 * Time: 3:08 PM
 */
public class DBHelper extends SQLiteOpenHelper {


    /**
     * KOSTANTA DATABASE
     */
    public static final int DB_VERSION = 1;
    public static final String DB_NAME = "budgetdb";
    /**
     * KONSTANTA TABEL HARIAN
     */
    public static final String DAILY_TABLE = "harian";
    public static final String D_COLUMN_ID = "id_harian";
    public static final String D_COLUMN_KETERANGAN = "keterangan";
    public static final String D_COLUMN_BALANCE = "balance";
    public static final String D_COLUMN_COST = "cost";
    public static final String D_COLUMN_DATE = "tanggal";
    /**
     * KONSTANTA TABEL BULANAN
     */
    public static final String MONTHLY_TABLE = "bulanan";
    public static final String M_COLUMN_ID = "id_bulanan";
    public static final String M_COLUMN_BULAN = "nama_bulan";
    public static final String M_COLUMN_TAHUN = "tahun";
    public static final String M_COLUMN_TOTAL = "total";
    private static final String DAILY_COLS_NAME[] = {D_COLUMN_ID, D_COLUMN_DATE, D_COLUMN_COST, D_COLUMN_KETERANGAN, D_COLUMN_BALANCE};
    private static final String MONTHLY_COLS_NAME[] = {M_COLUMN_ID, M_COLUMN_BULAN, M_COLUMN_TAHUN, M_COLUMN_TOTAL};
    SQLiteDatabase db;

    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DB_NAME, factory, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE_HARIAN = "" +
                "CREATE TABLE " + DAILY_TABLE + "(" +
                D_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                D_COLUMN_BALANCE + " INTEGER," +
                D_COLUMN_DATE + " TEXT," +
                D_COLUMN_COST + " INTEGER," +
                D_COLUMN_KETERANGAN + " TEXT" +
                ")";
        db.execSQL(CREATE_TABLE_HARIAN);

        String CREATE_TABLE_BULANAN = "" +
                "CREATE TABLE " + MONTHLY_TABLE + "(" +
                M_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                M_COLUMN_BULAN + " TEXT," +
                M_COLUMN_TAHUN + " TEXT," +
                M_COLUMN_TOTAL + " INTEGER" +
                ")";
        db.execSQL(CREATE_TABLE_BULANAN);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DAILY_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + MONTHLY_TABLE);
        onCreate(db);
    }

    public long getBalance() {
        long hasil = 0L;
        this.db = getReadableDatabase();
        String QUERY = "SELECT "+ D_COLUMN_BALANCE +" FROM " + DAILY_TABLE;
        Cursor cursor = this.db.rawQuery(QUERY, null);
        if (cursor.moveToFirst()) {
            hasil = cursor.getInt(0) > 0 ? cursor.getInt(0) : 0L;
        }
        this.db.close();
        return hasil;
    }

    public void setBalance(int balance) {
        this.db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(D_COLUMN_BALANCE, balance);

        this.db.insert(DAILY_TABLE, null, values);
        this.db.close();
    }

    public void addDaily(Daily daily) {
        this.db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(D_COLUMN_DATE, daily.date);
        values.put(D_COLUMN_COST, daily.cost);
        values.put(D_COLUMN_KETERANGAN, daily.description);

        this.db.insert(DAILY_TABLE, null, values);
        this.db.close();
    }

    public Daily findDaily(int i) {
        this.db = getReadableDatabase();

        Cursor cursor = this.db.query(
                DAILY_TABLE,
                DAILY_COLS_NAME,//column
                D_COLUMN_ID + " = ?",//where
                new String[]{String.valueOf(i)},//param
                null,//group by
                null,//having
                null//order by
        );

        if (!cursor.moveToFirst()) return null;

        Daily day = new Daily();
        day.id = cursor.getInt(0);
        day.date = cursor.getString(1);
        day.cost = cursor.getInt(2);
        day.description = cursor.getString(3);
        day.balance = cursor.getInt(4);

        this.db.close();
        return day;
    }

    public void deleteDaily(Daily daily) {
        this.db = getWritableDatabase();
        this.db.delete(DAILY_TABLE, D_COLUMN_ID + " = ?", new String[]{String.valueOf(daily.id)});
        this.db.close();
    }

    public long getTotalExpenditure() {
        long hasil = 0L;
        this.db = getReadableDatabase();
        String QUERY = "SELECT SUM(" + D_COLUMN_COST + ") FROM " + DAILY_TABLE;
        Cursor cursor = this.db.rawQuery(QUERY, null);
        if (cursor.moveToFirst()) {
            hasil = cursor.getInt(0) > 0 ? cursor.getInt(0) : 0L;
        }
        this.db.close();
        return hasil;
    }

    public List<Daily> findAllDailys() {
        List<Daily> dailies = new ArrayList<>();

        String QUERY = "SELECT * FROM " + DAILY_TABLE;
        this.db = getReadableDatabase();
        Cursor cursor = this.db.rawQuery(QUERY, null);

        if (cursor.moveToFirst()) {
            do {
                Daily daily = new Daily();
                daily.id = cursor.getInt(0);
                daily.balance = cursor.getInt(1);
                daily.date = cursor.getString(2);
                daily.cost = cursor.getInt(3);
                daily.description = cursor.getString(4);
                dailies.add(daily);
            } while (cursor.moveToNext());
        }
        this.db.close();
        return dailies;
    }

    public void addMonthly(Monthly monthly) {
        this.db = getWritableDatabase();

        ContentValues val = new ContentValues();
        val.put(M_COLUMN_BULAN, monthly.month);
        val.put(M_COLUMN_TAHUN, monthly.year);
        val.put(M_COLUMN_TOTAL, monthly.total);

        this.db.insert(MONTHLY_TABLE, null, val);
        this.db.close();
    }

    public List<Monthly> findAllMonthlys() {
        List<Monthly> monthlies = new ArrayList<>();

        String QUERY = "SELECT * FROM " + MONTHLY_TABLE + " ORDER BY " + M_COLUMN_TAHUN;
        this.db = getReadableDatabase();
        Cursor cursor = this.db.rawQuery(QUERY, null);

        if (cursor.moveToFirst()) {
            do {
                Monthly bulan = new Monthly();
                bulan.id = cursor.getInt(0);
                bulan.month = cursor.getString(1);
                bulan.year = cursor.getString(2);
                bulan.total = cursor.getLong(3);
                monthlies.add(bulan);
            } while (cursor.moveToNext());
        }
        this.db.close();
        return monthlies;
    }
}
