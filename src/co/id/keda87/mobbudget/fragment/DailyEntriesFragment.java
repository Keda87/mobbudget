package co.id.keda87.mobbudget.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import co.id.keda87.mobbudget.R;
import co.id.keda87.mobbudget.adapter.DailyAdapter;
import co.id.keda87.mobbudget.helper.DBHelper;
import co.id.keda87.mobbudget.pojo.Daily;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Keda87
 * Date: 12/22/13
 * Time: 1:55 PM
 */
public class DailyEntriesFragment extends Fragment{

    private ListView daftarPengeluaran;
    private DBHelper helper;
    private DailyAdapter dailyAdapter;
    private List<Daily> pengeluarans;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_daily_entries, container, false);

        daftarPengeluaran = (ListView) rootView.findViewById(R.id.daftarHarian);
        helper = new DBHelper(this.getActivity(), null, null, 1);
        pengeluarans = new ArrayList<>(helper.findAllDailys());
        dailyAdapter = new DailyAdapter(this.getActivity(), pengeluarans);
        daftarPengeluaran.setAdapter(dailyAdapter);

        return rootView;
    }
}
