package co.id.keda87.mobbudget.fragment;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import co.id.keda87.mobbudget.R;
import co.id.keda87.mobbudget.helper.DBHelper;
import co.id.keda87.mobbudget.pojo.Daily;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Keda87
 * Date: 12/21/13
 * Time: 2:13 PM
 */
public class FinanceFragment extends Fragment {

    private SimpleDateFormat sdf;
    private DBHelper helper;
    private EditText date, desc, cost;
    private Button tambahPengeluaran;
    private Date currentDate;
    private Toast success;
    private SharedPreferences pref;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_finance, container, false);

        date = (EditText) rootView.findViewById(R.id.etDate);
        desc = (EditText) rootView.findViewById(R.id.etDescriptions);
        cost = (EditText) rootView.findViewById(R.id.etCost);
        tambahPengeluaran = (Button) rootView.findViewById(R.id.addExpe);
        success = Toast.makeText(this.getActivity(), "Successfully Added", Toast.LENGTH_SHORT);
        pref = PreferenceManager.getDefaultSharedPreferences(this.getActivity());

        currentDate = new Date();
        sdf = new SimpleDateFormat("dd MMMM yyyy - h:mm a");
        date.setText(sdf.format(currentDate));
        date.setEnabled(false);
        helper = new DBHelper(this.getActivity(), null, null, 1);

        tambahPengeluaran.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String datex = date.getText().toString();
                String descx = desc.getText().toString();
                String costx = cost.getText().toString();

                if (datex.trim().length() == 0 || descx.trim().length() == 0 || costx.trim().length() == 0) return;

                Daily harian = new Daily();
                harian.date = datex;
                harian.description = descx;
                harian.cost = Integer.parseInt(costx);
                helper.addDaily(harian);

                //update preference & balance table
                long curBalance = Long.parseLong(pref.getString("balance", "0"));
                curBalance -= harian.cost;
                pref.edit().putString("balance", String.valueOf(curBalance)).commit();

                desc.setText("");
                cost.setText("");
                success.show();
            }
        });

        return rootView;
    }
}
