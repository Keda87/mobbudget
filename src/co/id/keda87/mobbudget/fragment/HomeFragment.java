package co.id.keda87.mobbudget.fragment;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import co.id.keda87.mobbudget.R;
import co.id.keda87.mobbudget.helper.DBHelper;
import co.id.keda87.mobbudget.helper.NumberToCurrency;

/**
 * Created with IntelliJ IDEA.
 * User: Keda87
 * Date: 12/21/13
 * Time: 2:00 PM
 */
public class HomeFragment extends Fragment {

    private TextView labelBalance, labelCost;
    private DBHelper helper;
    private SharedPreferences pref;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        labelBalance = (TextView) rootView.findViewById(R.id.labelCurrentBalance);
        labelCost = (TextView) rootView.findViewById(R.id.labelTotalCost);
        helper = new DBHelper(this.getActivity(),null, null, 1);
        pref = PreferenceManager.getDefaultSharedPreferences(this.getActivity());

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();

        //total cost
        String total = (helper.getTotalExpenditure() > 0) ? NumberToCurrency.currencyBuilder(helper.getTotalExpenditure()) : "-";
        labelCost.setText(total);

        //total balance
        labelBalance.setText(NumberToCurrency.currencyBuilder(Long.parseLong(pref.getString("balance", "0"))));
    }
}
