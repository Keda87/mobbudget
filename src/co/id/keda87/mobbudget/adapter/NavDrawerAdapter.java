package co.id.keda87.mobbudget.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import co.id.keda87.mobbudget.R;
import co.id.keda87.mobbudget.pojo.NavDrawer;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Keda87
 * Date: 12/21/13
 * Time: 12:47 AM
 */
public class NavDrawerAdapter extends BaseAdapter {

    private Context context;
    private List<NavDrawer> navDrawers;

    public NavDrawerAdapter(Context context, List<NavDrawer> navDrawers) {
        this.context = context;
        this.navDrawers = navDrawers;
    }

    @Override
    public int getCount() {
        return navDrawers.size();
    }

    @Override
    public Object getItem(int position) {
        return navDrawers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_drawer, null);
        }

        ImageView imgIcon = (ImageView) convertView.findViewById(R.id.icon);
        TextView txtTitle = (TextView) convertView.findViewById(R.id.title);

        imgIcon.setImageResource(navDrawers.get(position).icon);
        txtTitle.setText(navDrawers.get(position).title);

        return convertView;
    }
}
