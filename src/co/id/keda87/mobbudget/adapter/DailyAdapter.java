package co.id.keda87.mobbudget.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import co.id.keda87.mobbudget.R;
import co.id.keda87.mobbudget.helper.DBHelper;
import co.id.keda87.mobbudget.helper.NumberToCurrency;
import co.id.keda87.mobbudget.pojo.Daily;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Keda87
 * Date: 12/21/13
 * Time: 9:58 PM
 */
public class DailyAdapter extends BaseAdapter {

    private List<Daily> dailyList;
    private LayoutInflater inflater;

    public DailyAdapter(Context context, List<Daily> list) {
        this.inflater = LayoutInflater.from(context);
        this.dailyList = list;
    }

    @Override
    public int getCount() {
        return dailyList.size();
    }

    @Override
    public Object getItem(int position) {
        return dailyList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_daily, null);
            holder = new ViewHolder();
            holder.labelDate = (TextView) convertView.findViewById(R.id.tvDate);
            holder.labelDesc = (TextView) convertView.findViewById(R.id.tvDesc);
            holder.labelCost = (TextView) convertView.findViewById(R.id.tvCost);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Daily currentDay = dailyList.get(position);
        holder.labelDate.setText(currentDay.date);
        holder.labelDesc.setText(currentDay.description);
        holder.labelCost.setText(NumberToCurrency.currencyBuilder(currentDay.cost));
        return convertView;
    }

    static class ViewHolder {
        TextView labelCost;
        TextView labelDate;
        TextView labelDesc;
    }
}
