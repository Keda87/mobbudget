package co.id.keda87.mobbudget.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import co.id.keda87.mobbudget.R;
import co.id.keda87.mobbudget.helper.DBHelper;
import co.id.keda87.mobbudget.helper.NumberToCurrency;
import co.id.keda87.mobbudget.pojo.Monthly;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Keda87
 * Date: 12/22/13
 * Time: 5:11 PM
 */
public class MonthlyAdapter extends BaseAdapter {

    private List<Monthly> monthlyList;
    private LayoutInflater inflater;
    private DBHelper db;

    public MonthlyAdapter(Context context, List<Monthly> monthlyList, DBHelper db) {
        this.monthlyList = monthlyList;
        this.db = db;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return monthlyList.size();
    }

    @Override
    public Object getItem(int position) {
        return monthlyList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_monthly, null);
            holder = new ViewHolder();
            holder.labelTotal = (TextView) convertView.findViewById(R.id.tvTotalBulanan);
            holder.labelBulan = (TextView) convertView.findViewById(R.id.tvBulanan);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Monthly month = monthlyList.get(position);
        holder.labelTotal.setText(NumberToCurrency.currencyBuilder(month.total));
        holder.labelBulan.setText(month.month);
        return convertView;
    }

    static class ViewHolder {
        TextView labelTotal;
        TextView labelBulan;
    }
}
