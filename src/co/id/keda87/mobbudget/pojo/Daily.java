package co.id.keda87.mobbudget.pojo;

/**
 * Created with IntelliJ IDEA.
 * User: Keda87
 * Date: 12/21/13
 * Time: 1:12 AM
 */
public class Daily {

    public int id;
    public int balance;
    public int cost;
    public String description;
    public String date;

}
