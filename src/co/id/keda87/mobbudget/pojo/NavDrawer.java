package co.id.keda87.mobbudget.pojo;

/**
 * Created with IntelliJ IDEA.
 * User: Keda87
 * Date: 12/21/13
 * Time: 12:43 AM
 */
public class NavDrawer {

    public String title;
    public int icon;

    public NavDrawer(String title, int icon) {
        this.title = title;
        this.icon = icon;
    }
}
