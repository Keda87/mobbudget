package co.id.keda87.mobbudget.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import co.id.keda87.mobbudget.R;

public class MyActivity extends Activity {

    private SharedPreferences pref;
    private EditText user, pass;
    private Toast message;
    private String uname, pwd;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        user = (EditText) findViewById(R.id.user);
        pass = (EditText) findViewById(R.id.pass);
        message = Toast.makeText(getApplicationContext(),"", Toast.LENGTH_SHORT);
    }

    @Override
    protected void onStart() {
        super.onStart();
        pref = PreferenceManager.getDefaultSharedPreferences(this);
    }

    private boolean isAuth() {
        boolean status = false;
        uname = user.getText().toString().trim();
        pwd = pass.getText().toString().trim();

        if(uname.equals(pref.getString("username", "")) && pwd.equals(pref.getString("password", ""))) {
            status = true;
        }

        return status;
    }

    public void doLogin(View view) {
        Intent intent = new Intent(this, BaseActivity.class);

        if (isAuth()) {
            startActivity(intent);
            message.setText("Login Success");
            message.show();
        } else {
            message.setText("Invalid Login");
            message.show();
        }
        user.setText("");
        pass.setText("");
    }
}
