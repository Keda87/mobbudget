package co.id.keda87.mobbudget.activity;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import co.id.keda87.mobbudget.R;
import co.id.keda87.mobbudget.adapter.NavDrawerAdapter;
import co.id.keda87.mobbudget.fragment.DailyEntriesFragment;
import co.id.keda87.mobbudget.fragment.FinanceFragment;
import co.id.keda87.mobbudget.fragment.HomeFragment;
import co.id.keda87.mobbudget.pojo.NavDrawer;
import co.id.keda87.mobbudget.preference.SettingPreference;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Keda87
 * Date: 12/21/13
 * Time: 12:34 PM
 */
public class BaseActivity extends Activity {

    private DrawerLayout mDrawerlayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    //slider
    private String[] navMenuTitles;
    private TypedArray navMenuIcons;
    private List<NavDrawer> navDrawers;
    private NavDrawerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_activity);

        mTitle = mDrawerTitle = getTitle();

        //load slider menu items
        navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);
        //load slider icons
        navMenuIcons = getResources().obtainTypedArray(R.array.nav_drawer_icons);

        mDrawerlayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.list_slidermenu);
        navDrawers = new ArrayList<>();

        navDrawers.add(new NavDrawer(navMenuTitles[0], navMenuIcons.getResourceId(0, -1)));
        navDrawers.add(new NavDrawer(navMenuTitles[1], navMenuIcons.getResourceId(1, -1)));
        navDrawers.add(new NavDrawer(navMenuTitles[2], navMenuIcons.getResourceId(2, -1)));
        navDrawers.add(new NavDrawer(navMenuTitles[3], navMenuIcons.getResourceId(3, -1)));
        navDrawers.add(new NavDrawer(navMenuTitles[4], navMenuIcons.getResourceId(4, -1)));

        navMenuIcons.recycle();

        adapter = new NavDrawerAdapter(getApplicationContext(), navDrawers);
        mDrawerList.setAdapter(adapter);

        //enable actionbar
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerlayout, R.drawable.ic_drawer, R.string.app_name, R.string.app_name) {

            public void onDrawerClosed(View view) {
                getActionBar().setTitle(mTitle);
                invalidateOptionsMenu();
            }

            public void onDrawerOpened() {
                getActionBar().setTitle(mDrawerTitle);
                invalidateOptionsMenu();
            }
        };
        mDrawerlayout.setDrawerListener(mDrawerToggle);

        if (savedInstanceState == null) {
            displayView(0);
        }
        mDrawerList.setOnItemClickListener(new SlideMenuClickListener());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean drawerOpen = mDrawerlayout.isDrawerOpen(mDrawerList);
        menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    private void displayView(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new HomeFragment();
                break;
            case 1:
                fragment = new FinanceFragment();
                break;
            case 2:
                fragment = new DailyEntriesFragment();
                break;
            case 3:
                break;
            case 4:
                fragment = new SettingPreference();
                break;
            default:
                break;
        }
        if (fragment != null) {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).commit();

            //update display
            mDrawerList.setItemChecked(position, true);
            mDrawerList.setSelection(position);
            setTitle(navMenuTitles[position]);
            mDrawerlayout.closeDrawer(mDrawerList);
        } else {
            Log.e("KESALAHAN", "GAGAL MEMBUAT FRAGMENT");
        }
    }

    private class SlideMenuClickListener implements ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            displayView(position);
        }
    }
}

